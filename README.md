# Windows环境GCC小程序测试专用

```bash
.
├── Makefile
├── README.md
├── tex.c
└── 新建.bat	# 双击打开命令行
```

# Makefile

```makefile
all : tex

# 请根据自己的工程修改相应的选项内容,文件为*nix(unix,Linux)格式,可用Notepad++等打开
# Windows系统记事本打开不会换行

# 要编译的常用参数选项
CFLAGS=	-Wall -pedantic -O2

#要编译的文件名
obj = tex
#要编译文件后缀
suffix = .c
#要使用的编译器(gcc或g++)
cc = gcc
#编译标准(C:-std=c11,C++:-std=c++14)
Std = -std=c11
#要编译成32或64位程序
Bit = -m32

CFLAGS+= $(Std)
CFLAGS+= $(Bit)

# 下边是添加要执行的命令,每行前边必须有个 [Tab]键
tex : $(obj)$(suffix)
	$(cc) $(obj)$(suffix) $(CFLAGS) -o $(obj)
	@./$(obj)
	@echo Error : %ERRORLEVEL%

.PHONY : clean
clean:
	-del $(obj).exe
```

